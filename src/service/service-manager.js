const { sentryReport } = require('../utils/sentry.js');

let serviceList = [];

function addToRunningService(service) {
    try {
        serviceList.push(service);
    } catch(err) {
        sentryReport(err);
    }
}

function stopRunningServiceExclude(service) {
    serviceList.forEach(runningService => {
        if (runningService !== service) {
            runningService.stop()
                .then(() => {
                    _removeFromServiceList(service);
                })
                .catch(err => {
                    sentryReport(err);
                });
        }
    });
}

function stopAllRunningService() {
    serviceList.forEach(service => {
        service.stop()
            .then(() => {
                _removeFromServiceList(service);
            })
            .catch(err => {
                sentryReport(err);
            });
    });
}

function _removeFromServiceList(service) {
    try {
        let svcIndex = serviceList.indexOf(service);
        if (svcIndex !== -1) {
            serviceList.splice(svcIndex, 1);
        }
    } catch(err) {
        sentryReport(err);
    }
}

module.exports = {
    addToRunningService,
    stopRunningServiceExclude,
    stopAllRunningService
}