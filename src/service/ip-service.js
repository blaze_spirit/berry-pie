const config     = require('../config/config.js');
const internalIp = require('internal-ip');
const firebase   = require('firebase/app');
const { raven, sentryReport } = require('../utils/sentry.js');
require('firebase/database');

raven.context(() => {
    firebase.initializeApp(config.FIREBASE_CONFIG);

    let currentIP = '';
    let database = firebase.database();

    // get internal ip
    getIpAndUpdateFirebase();

    // periodically check and update ip to firebase.
    setInterval(getIpAndUpdateFirebase, config.IP_POLLING_INTERVAL);

    async function getIpAndUpdateFirebase() {
        try {
            let ip = await _getInternalIp();
            if (ip != currentIP) {
                _updateFirebaseInternalIp(ip);
            }
        } catch(err) {
            sentryReport(err);
        }
    }

    // private functions
    function _getInternalIp() {
        return new Promise((resolve, reject) => {
            internalIp.v4().then(ip => {
                if (ip) {
                    resolve(ip);
                }
                else {
                    reject(new Error('Failed to obtain internal IP address. Possibly disconnected from network.'));
                }
            });
        });
    }

    function _updateFirebaseInternalIp(ip) {
        return new Promise((resolve, reject) => {
            database
                .ref('internal_ip')
                .set(ip)
                .then(() => {
                    currentIP = ip;
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
});
