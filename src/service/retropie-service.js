const spawn = require('child_process').spawn;
const kill  = require('tree-kill');
const { sentryReport } = require('../utils/sentry.js');

let retropie = null;

function start() {
    return new Promise((resolve, reject) => {
        if (retropie) { // return if service already running.
            resolve();
            return;
        }
        retropie = spawn('emulationstation');
        if (retropie.pid) {
            resolve();
        }
        retropie.on('error', (err) => {
            reject(err);
            sentryReport(err);
        });
    })
}

function stop() {
    return new Promise((resolve, reject) => {
        if (retropie) {
            kill(retropie.pid, (err) => {
                if (err) {
                    reject(err);
                    sentryReport(err);
                }
                retropie = null;
                resolve();
            });
        }
        else {
            resolve();
        }
    });
}

module.exports = {
    start,
    stop
};
