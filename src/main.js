const config    = require('./config/config.js');
const express   = require('express');
const http      = require('http');
const helmet    = require('helmet');
const { raven } = require('./utils/sentry.js');
const retropie  = require('./service/retropie-service.js');
const kodi      = require('./service/kodi-service.js');
const serviceManager = require('./service/service-manager.js');
require('./service/ip-service.js');

const server = express();

server.use(raven.requestHandler());
server.use(helmet());

// request endpoints
// ==================== RetroPie ====================
server.get('/retropie-start', function (req, res) {
    serviceManager.stopRunningServiceExclude(retropie);
    retropie.start()
        .then(() => {
            serviceManager.addToRunningService(retropie);
            res.status(200).send('RetroPie started.')
        })
        .catch(err => {
            res.status(500).send(err);
        });
});

server.get('/retropie-stop', function (req, res) {
    retropie.stop()
        .then(() => {
            res.status(200).send('RetroPie stoped.')
        })
        .catch(err => {
            res.status(500).send(err);
        });
});

// ==================== Kodi ====================
server.get('/kodi-start', function (req, res) {
    serviceManager.stopRunningServiceExclude(kodi);
    kodi.start()
        .then(() => {
            serviceManager.addToRunningService(kodi);
            res.status(200).send('Kodi started.')
        })
        .catch(err => {
            res.status(500).send(err);
        });
});

server.get('/kodi-stop', function (req, res) {
    kodi.stop()
        .then(() => {
            res.status(200).send('Kodi stoped.')
        })
        .catch(err => {
            res.status(500).send(err);
        });
});

server.use(raven.errorHandler());

http.createServer(server).listen(config.PORT, () => {
    console.log(`Listening on port ${config.PORT}`);
});