const spawn = require('child_process').spawn;
const kill  = require('tree-kill');
const { sentryReport } = require('../utils/sentry.js');

let kodi = null;

function start() {
    return new Promise((resolve, reject) => {
        if (kodi) { // return if service already running.
            resolve();
            return;
        }
        kodi = spawn('kodi');
        if (kodi.pid) {
            resolve();
        }
        kodi.on('error', (err) => {
            reject(err);
            sentryReport(err);
        });
    })
}

function stop() {
    return new Promise((resolve, reject) => {
        if (kodi) {
            kill(kodi.pid, (err) => {
                if (err) {
                    reject(err);
                    sentryReport(err);
                }
                kodi = null;
                resolve();
            });
        }
        else {
            resolve();
        }
    });
}

module.exports = {
    start,
    stop
};
