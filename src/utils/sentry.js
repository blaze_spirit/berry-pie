const raven = require('raven');

raven.config('https://e04ad03328884b9d9855da7dfd40a82b@sentry.io/1229256', {
    captureUnhandledRejections: true
}).install();

raven.on('error', (err) => {
    console.info(`Reason: ${err.reason}`);
    console.info(`Status code: ${err.statusCode}`);
    console.info(`Response: ${err.response}`);
});

function sentryReport(err) {
    raven.captureException(err, (sendErr, eventId) => {
        if (sendErr) {
            console.error('Failed to send captured exception to Sentry')
        }
        else {
            console.info(`Exception sent to Sentry [ eventId: ${eventId} ]`)
        }
    });
}

module.exports = {
    raven,
    sentryReport
};
