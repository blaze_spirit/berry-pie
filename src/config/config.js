// server
const PORT = 3000;

// firebase
const FIREBASE_CONFIG = {
    apiKey: "AIzaSyADmTjUBk2Gka6gsWsQMPUyTnZOvjT786w",
    authDomain: "berry-pie-blaze.firebaseapp.com",
    databaseURL: "https://berry-pie-blaze.firebaseio.com",
    projectId: "berry-pie-blaze",
    storageBucket: "berry-pie-blaze.appspot.com",
    messagingSenderId: "733087683623"
};

// winston & loggly config
const WINSTON_CONSOLE_CONFIG = {
    json: true,
    colorize: true
};

const WINSTON_LOGGLY_CONFIG = {
    subdomain: 'blazespirit',
    token: '5b770780-392a-4b30-9798-7804595d1048',
    json: true,
    tags: ["berry-pie-main"]
};

// ip-service
const IP_POLLING_INTERVAL = 5000;

module.exports = {
    PORT,
    FIREBASE_CONFIG,
    WINSTON_CONSOLE_CONFIG,
    WINSTON_LOGGLY_CONFIG,
    IP_POLLING_INTERVAL
}